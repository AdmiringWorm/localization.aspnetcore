using NUnit.Framework;

namespace AspNetCore.JsonLocalizer.Tests
{
	[TestFixture]
	public class Class1
	{
		[Test]
		public void PassingTest()
		{
			Assert.That(Add(2, 2), Is.EqualTo(4));
		}

		[Test]
		public void FailingTest()
		{
			Assert.That(Add(2, 2), Is.EqualTo(5));
		}

		private int Add(int x, int y)
		{
			return x + y;
		}
	}
}
