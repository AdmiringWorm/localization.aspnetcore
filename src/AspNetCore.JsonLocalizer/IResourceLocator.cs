﻿using System;
using System.Collections.Generic;
using System.Globalization;

namespace AspNetCore.JsonLocalizer
{
	/// <summary>
	///   Represents a service that provides a collection of resource strings
	///   from a file.
	/// </summary>
	public interface IResourceLocator
	{
		/// <summary>
		///   Locates the resource path for the given <paramref name="type" />
		///   and <paramref name="culture" />.
		/// </summary>
		/// <param name="type">   The type to locate resources for.</param>
		/// <param name="culture">The culture to locate resources for.</param>
		/// <returns>
		///   The path to the resource, or <see langword="null" /> if no resource
		///   path was found.
		/// </returns>
		/// <remarks>
		///   <note type="note">If the <paramref name="culture" /> is
		///   <see langword="null" /> then this method will locate the default
		///   path for the resource.</note>
		/// </remarks>
		string LocateResourcePath(Type type, CultureInfo culture);

		/// <summary>
		///   Loads the resource and returns a
		///   <see cref="IDictionary{TKey, TValue}" /> with the existing values.
		/// </summary>
		/// <param name="path">
		///   The path to load the resource for (should not be
		///   <see langword="null" />).
		/// </param>
		/// <returns>The populated values.</returns>
		IDictionary<string, string> LoadResource(string path);
	}
}
